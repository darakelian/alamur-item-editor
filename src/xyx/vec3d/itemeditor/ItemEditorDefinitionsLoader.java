package xyx.vec3d.itemeditor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ItemEditorDefinitionsLoader implements Serializable {

	private static final long serialVersionUID = 4626545171737371077L;

	private List<ItemEditorItemDefinition> defs;
	
	public ItemEditorDefinitionsLoader() {
		defs = new ArrayList<ItemEditorItemDefinition>();
	}

	public List<ItemEditorItemDefinition> getDefs() {
		return defs;
	}

	public void setDefs(List<ItemEditorItemDefinition> defs) {
		this.defs = defs;
	}
	
	public void addItemDef(ItemEditorItemDefinition def) {
		defs.add(def);
	}

}
