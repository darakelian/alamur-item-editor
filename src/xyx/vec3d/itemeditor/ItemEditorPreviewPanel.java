package xyx.vec3d.itemeditor;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ItemEditorPreviewPanel extends JPanel implements KeyListener {

	private static final long serialVersionUID = 1165501183933178534L;

	private ItemEditorFrame parentFrame;
	
	public ItemEditorPreviewPanel(ItemEditorFrame parentFrame) {
		this.parentFrame = parentFrame;
	}
	
	@Override
	public void keyPressed(KeyEvent ke) {
		if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
			System.exit(0);
		}
	}

	@Override
	public void keyReleased(KeyEvent ke) {
		
	}

	@Override
	public void keyTyped(KeyEvent ke) {
		
	}
	
}
