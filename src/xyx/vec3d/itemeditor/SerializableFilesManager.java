package xyx.vec3d.itemeditor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ConcurrentModificationException;

public class SerializableFilesManager {

	public synchronized static ItemEditorDefinitionsLoader loadItemDefs() {
		try {
			return (ItemEditorDefinitionsLoader) loadSerializedFile(new File("data/itemdefs.v3d"));
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public synchronized static void saveItemDefs(ItemEditorDefinitionsLoader itemDefs) {
		try {
			storeSerializableClass(itemDefs, new File("data/itemdefs.v3d"));
		} catch (ConcurrentModificationException e) {
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public static final Object loadSerializedFile(File f) throws IOException,
			ClassNotFoundException {
		if (!f.exists())
			return null;
		ObjectInputStream in = new ObjectInputStream(new FileInputStream(f));
		Object object = in.readObject();
		in.close();
		return object;
	}

	public static final void storeSerializableClass(Serializable o, File f)
			throws IOException {
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(f));
		out.writeObject(o);
		out.close();
	}
}
