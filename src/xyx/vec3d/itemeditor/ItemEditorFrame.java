package xyx.vec3d.itemeditor;

import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class ItemEditorFrame extends JFrame implements KeyListener {

	private static final long serialVersionUID = -3671029896400607310L;
	
	private ItemEditorListPanel itemList;
	private ItemEditorPreviewPanel itemPreview;
	private ItemEditorStatsPanel itemStats;
	
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem menuItem;
	
	private ItemEditorDefinitionsLoader itemDefs;
	
	public ItemEditorFrame() {
		this.setExtendedState(Frame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setUndecorated(true);
		this.addKeyListener(this);
		this.setLayout(new GridLayout(1, 3));
		
		File defs = new File("data/itemdefs.v3d");
		if (!defs.exists()) {
			itemDefs = new ItemEditorDefinitionsLoader();
			SerializableFilesManager.saveItemDefs(itemDefs);
		} else {
			itemDefs = SerializableFilesManager.loadItemDefs();
		}
		
		itemList = new ItemEditorListPanel(this);
		itemPreview = new ItemEditorPreviewPanel(this);
		itemStats = new ItemEditorStatsPanel(this);
		this.add(itemList);
		this.add(itemPreview);
		this.add(itemStats);
		
		this.setUpMenu();
		this.setJMenuBar(menuBar);
	}

	@Override
	public void keyPressed(KeyEvent ke) {
		if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
			System.exit(0);
		}
	}

	@Override
	public void keyReleased(KeyEvent ke) {
		
	}

	@Override
	public void keyTyped(KeyEvent ke) {
		
	}

	public ItemEditorListPanel getItemList() {
		return itemList;
	}

	public void setItemList(ItemEditorListPanel itemList) {
		this.itemList = itemList;
	}

	public ItemEditorPreviewPanel getItemPreview() {
		return itemPreview;
	}

	public void setItemPreview(ItemEditorPreviewPanel itemPreview) {
		this.itemPreview = itemPreview;
	}

	public ItemEditorStatsPanel getItemStats() {
		return itemStats;
	}

	public void setItemStats(ItemEditorStatsPanel itemStats) {
		this.itemStats = itemStats;
	}
	
	public ItemEditorDefinitionsLoader getItemDefs() {
		return itemDefs;
	}
	
	private void setUpMenu() {
		menuBar = new JMenuBar();
		menu = new JMenu("Options");
		menu.setMnemonic(KeyEvent.VK_O);
		menuBar.add(menu);
		
		menuItem = new JMenuItem("Save");
		menuItem.addActionListener(new MenuActionListener());
		menuItem.setMnemonic(KeyEvent.VK_S);
		menuItem.setActionCommand("save");
		menu.add(menuItem);
		
		menuItem = new JMenuItem("Load");
		menuItem.addActionListener(new MenuActionListener());
		menuItem.setMnemonic(KeyEvent.VK_L);
		menuItem.setActionCommand("load");
		menu.add(menuItem);
		
		menu.addSeparator();
		
		menuItem = new JMenuItem("Exit");
		menuItem.addActionListener(new MenuActionListener());
		menuItem.setMnemonic(KeyEvent.VK_E);
		menuItem.setActionCommand("exit");
		menu.add(menuItem);
	}
	
	private class MenuActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String command = e.getActionCommand().toLowerCase();
			switch (command) {
			case "save":
				SerializableFilesManager.saveItemDefs(itemDefs);
				break;
			case "load":
				break;
			case "exit":
				System.exit(0);
				break;
				default:
					System.out.println("Selected: " + e.getActionCommand());
					break;
			}
		}
		
	}

}
