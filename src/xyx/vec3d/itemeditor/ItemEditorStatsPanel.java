package xyx.vec3d.itemeditor;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Set;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

public class ItemEditorStatsPanel extends JPanel implements KeyListener {
	
	public static final long serialVersionUID = 2365279083932316534L;
	
	private ItemEditorFrame parentFrame;
	
	private ItemEditorItemDefinition currentModifyingItemDef;
	
	private DefaultListModel<Object> itemEditingModel;
	private JList<Object> itemEditingAttributes;
	
	public ItemEditorStatsPanel(ItemEditorFrame parentFrame) {
		this.parentFrame = parentFrame;
		this.setLayout(new FlowLayout());
		JButton newItem = new JButton("Create item");
		newItem.setActionCommand("create_item");
		newItem.addActionListener(new ButtonActionListener());
		add(newItem);
		JButton newAttribute = new JButton("Add attribute");
		newAttribute.setActionCommand("new_attribute");
		newAttribute.addActionListener(new ButtonActionListener());
		add(newAttribute);
		
		itemEditingModel = new DefaultListModel<Object>();
		itemEditingAttributes = new JList<Object>(itemEditingModel);
		itemEditingAttributes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		itemEditingAttributes.setLayoutOrientation(JList.VERTICAL);
		itemEditingAttributes.addMouseListener(new MouseAdapter() {
		    @SuppressWarnings("unchecked")
			public void mouseClicked(MouseEvent evt) {
		        JList<Object> list = (JList<Object>) evt.getSource();
		        if (evt.getClickCount() == 2) {
		            int index = list.locationToIndex(evt.getPoint());
		            doubleClick(index);
		        }
		    }
		});
		JScrollPane attributeScroller = new JScrollPane(itemEditingAttributes);
		add(attributeScroller);
	}
	
	public final void doubleClick(int index) {
		Attribute doubleClickedAttribute = (Attribute) itemEditingAttributes.getModel().getElementAt(index);
		Object value = doubleClickedAttribute.getValue();
		Object newValue = JOptionPane.showInputDialog(this, "Enter new value:", value);
		doubleClickedAttribute.setValue(newValue);
		doubleClickedAttribute.updatePrettyName();
		currentModifyingItemDef.getValues().put(doubleClickedAttribute.getKey(), newValue);
		parentFrame.getItemList().getModel().set(parentFrame.getItemList().getJList().getSelectedIndex(), currentModifyingItemDef);
	}
	
	public void updateItem(ItemEditorItemDefinition def) {
		itemEditingModel.clear();
		currentModifyingItemDef = def;
		HashMap<String, Object> values = (HashMap<String, Object>) def.getValues();
		Set<String> keys = values.keySet();
		String[] keyValues = keys.toArray(new String[keys.size()]);
		for (String key : keyValues) {
			Object value = values.get(key);
			Attribute attribute = new Attribute(key, value);
			itemEditingModel.addElement(attribute);
		}
	}
	
	@Override
	public void keyPressed(KeyEvent ke) {
		if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
			System.exit(0);
		}
	}

	@Override
	public void keyReleased(KeyEvent ke) {
		
	}

	@Override
	public void keyTyped(KeyEvent ke) {
		
	}
	
	public class Attribute {
		
		private String key, prettyName;
		
		private Object value;
		
		public Attribute(String key, Object value) {
			this.setKey(key);
			this.setValue(value);
			this.setPrettyName(beautifyName(key));
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getPrettyName() {
			return prettyName;
		}

		public void setPrettyName(String prettyName) {
			this.prettyName = prettyName;
		}

		public Object getValue() {
			return value;
		}

		public void setValue(Object value) {
			this.value = value;
		}
		
		@Override
		public String toString() {
			return getPrettyName();
		}
		
		public void updatePrettyName() {
			String oldPart = prettyName.substring(0, prettyName.lastIndexOf(":") + 2);
			String newPart = (String) getValue();
			prettyName = oldPart + newPart;
		}
		
		public String beautifyName(String key) {
			String prettyName = key.replaceFirst("i", "Item");
			prettyName = prettyName.replace("_", " ");
			prettyName = prettyName + ": " + (String) value;
			return prettyName;
		}
		
	}
	
	private final class ButtonActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ae) {
			switch (ae.getActionCommand().toLowerCase()) {
			case "create_item":
				ItemEditorItemDefinition def = new ItemEditorItemDefinition();
				def.getValues().put("i_name", "New item");
				parentFrame.getItemList().getModel().addElement(def);
				parentFrame.getItemDefs().addItemDef(def);
				break;
			case "new_attribute":
				if (currentModifyingItemDef == null) {
					JOptionPane.showMessageDialog(parentFrame.getItemStats(), "Please select an item first!", "Vec3d Item Editor", JOptionPane.ERROR_MESSAGE);
					return;
				}
				String text = JOptionPane.showInputDialog(parentFrame.getItemStats(), "Enter new tag like tag_name,value");
				String[] tokens = text.split(",", 2);
				String key = tokens[0];
				Object value = tokens[1];
				Attribute newAttribute = new Attribute(key, value);
				itemEditingModel.addElement(newAttribute);
				currentModifyingItemDef.getValues().put(key, value);
				parentFrame.getItemList().getModel().set(parentFrame.getItemList().getJList().getSelectedIndex(), currentModifyingItemDef);
				break;
			}
		}
		
	}
}
