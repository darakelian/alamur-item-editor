package xyx.vec3d.itemeditor;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ItemEditorListPanel extends JPanel implements KeyListener {

	private static final long serialVersionUID = 2966530933701419520L;
	
	private DefaultListModel<Object> listLoadedItems;
	private JList<Object> loadedItems;
	private ItemEditorFrame parentFrame;
	
	public ItemEditorListPanel(ItemEditorFrame parentFrame) {
		this.parentFrame = parentFrame;
		this.setLayout(new BorderLayout());
		listLoadedItems = new DefaultListModel<Object>();
		//Load items into the list
		File itemData = new File("data/itemdefs.v3d");
		if (!itemData.exists()) {
			JOptionPane.showMessageDialog(null, "No item definitions found; file is either corrupt or missing!", 
					"Vec3d Item Editor", JOptionPane.INFORMATION_MESSAGE);
		} else {
			for (ItemEditorItemDefinition def : parentFrame.getItemDefs().getDefs()) {
				listLoadedItems.addElement(def);
			}
		}
		loadedItems = new JList<Object>(listLoadedItems);
		loadedItems.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		loadedItems.setLayoutOrientation(JList.VERTICAL);
		loadedItems.addListSelectionListener(new SelectionListener());
		JScrollPane loadedItemsScrollPane = new JScrollPane(loadedItems);
		add(loadedItemsScrollPane, BorderLayout.CENTER);
	}
	
	@Override
	public void keyPressed(KeyEvent ke) {
		if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
			System.exit(0);
		}
	}

	@Override
	public void keyReleased(KeyEvent ke) {
		
	}

	@Override
	public void keyTyped(KeyEvent ke) {
		
	}
	
	public DefaultListModel<Object> getModel() {
		return listLoadedItems;
	}
	
	public JList<Object> getJList() {
		return loadedItems;
	}
	
	private final class SelectionListener implements ListSelectionListener {

		@Override
		public void valueChanged(ListSelectionEvent e) {
			if (!e.getValueIsAdjusting()) {
				ItemEditorStatsPanel statsPanel = parentFrame.getItemStats();
				statsPanel.updateItem((ItemEditorItemDefinition) loadedItems.getSelectedValue());
			}
		}
		
	}

}
