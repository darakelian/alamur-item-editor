package xyx.vec3d.itemeditor;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Class to hold the data an item can have, every item will have one of these
 * and an ItemDefinitions class will handle de/serializing them.
 * 
 * @author Daron
 *
 */
public class ItemEditorItemDefinition implements Serializable {

	private static final long serialVersionUID = -8550239179581768096L;
	
	private Map<String, Object> values;
	
	public ItemEditorItemDefinition() {
		values = new LinkedHashMap<String, Object>();
	}
	
	public Map<String, Object> getValues() {
		return values;
	}
	
	@Override
	public String toString() {
		return values.get("i_name") == null ? "No defs" : (String) values.get("i_name");
	}

}
